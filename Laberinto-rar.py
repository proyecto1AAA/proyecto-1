#! usr/bin/env python3
import random
import os

def genercamin(a,b):
    ran = random.randint(1,2)
    if ran > 1:
        for i in range(5,20):
            laberinto[19][i] = '*'
    else:
        for i in range(5,20):
            laberinto[i][19] = '*' 
    laberinto[19][19] = 'X'
    ran = random.randint(1,2)
    if ran > 1:
        for i in range(0,20):
            laberinto[i][0] = '*'
    else:
        for i in range(0,16):
            laberinto[a][i] = '*' 
    laberinto[a][a] = 'F'

def movdirec(cardinalidad):
    verificador = int(2)
    ubicaplayer = int(0) #Busca a X en el laberinto
    if cardinalidad == 'W' or cardinalidad == 'w':
        if 'X' in laberinto[0]:
            ubicaplayer = laberinto[0].index('X')
            os.system ("clear")
            print('Se movio hacia arriba')
            print('Ha chocado con una pared del laberinto')
        else:
            os.system ("clear")
            print('Se movio hacia arriba')
            for i in range(0,20):
                if laberinto[i].count('X') == 1:
                    ubicaplayer = laberinto[i].index('X')
                    if laberinto[i-1][ubicaplayer] == '*' or laberinto[i-1][ubicaplayer] == 'F':
                        laberinto[i-1][ubicaplayer] = 'X'
                        laberinto[i][ubicaplayer] = '*'
                    else:
                        print('Ha chocado con una pared del laberinto')
    if cardinalidad == 'S' or cardinalidad == 's':
        if 'X' in laberinto[19]:
            os.system ("clear")
            print('Se movio hacia abajo')
            print('Ha chocado con una pared del laberinto')
        else:
            os.system ("clear")
            print('Se movio hacia abajo')
            for i in range(19,-1,-1):
                if laberinto[i].count('X') == 1:
                    ubicaplayer = laberinto[i].index('X')
                    if laberinto[i+1][ubicaplayer] == '*':
                        laberinto[i+1][ubicaplayer] = 'X'
                        laberinto[i][ubicaplayer] = '*'
                        i = 0
                    else:
                        print('Ha chocado con una pared del laberinto')
                        i = 0
    if cardinalidad == 'A' or cardinalidad == 'a':
        for i in range(0,20):
            if laberinto[i][0] == 'X':
                verificador = 1
                os.system ("clear")
                print('Se movio hacia la izquierda')
                print('Ha chocado con una pared del laberinto')
        if verificador == 2:
            os.system ("clear")
            print('Se movio hacia la izquierda')
            for i in range(0,20):
                if laberinto[i].count('X') == 1:
                    ubicaplayer = laberinto[i].index('X')
                    if laberinto[i][ubicaplayer-1] == '*' or laberinto[i][ubicaplayer-1] == 'F':
                        laberinto[i][ubicaplayer-1] = 'X'
                        laberinto[i][ubicaplayer] = '*'
                    else:
                        print('Ha chocado con una pared del laberinto')
    if cardinalidad == 'D' or cardinalidad == 'd':
        for i in range(0,20):
            if laberinto[i][19] == 'X':
                verificador = 1
                os.system ("clear")
                print('Se movio hacia la derecha')
                print('Ha chocado con una pared del laberinto')
        if verificador == 2:
            os.system ("clear")
            print('Se movio hacia la derecha')
            for i in range(0,20):
                if laberinto[i].count('X') == 1:
                    ubicaplayer = laberinto[i].index('X')
                    if laberinto[i][ubicaplayer+1] == '*':
                        laberinto[i][ubicaplayer+1] = 'X'
                        laberinto[i][ubicaplayer] = '*'
                    else:
                        print('Ha chocado con una pared del laberinto')

def generconex(vertical,horizontal):
    #Cuadrante1
    #Vertical
    ran = random.randint(2,9)
    for i in range(0,16):
        laberinto[i][ran] = '*'
    #Horizontal
    ran = random.randint(2,9)
    for i in range(0,16):
        laberinto[ran][i] = '*'
    #Cuadrante2
    #Vertical
    ran = random.randint(11,18)
    for i in range(0,16):
        laberinto[i][ran] = '*'
    #Horizontal
    ran = random.randint(11,18)
    for i in range(0,16):
        laberinto[ran][i] = '*'
    #Cuadrante3
    #Vertical
    ran = random.randint(2,9)
    for i in range(4,20):
        laberinto[i][ran] = '*'
    #Horizontal
    ran = random.randint(2,9)
    for i in range(4,20):
        laberinto[ran][i] = '*'
    #Cuadrante4
    #Vertical
    ran = random.randint(11,18)
    for i in range(4,20):
        laberinto[i][ran] = '*'
    #Horizontal
    ran = random.randint(11,18)
    for i in range(4,20):
        laberinto[ran][i] = '*'
    #Retorna laberinto

    #Camino ultravertical
    ran = random.randint(2,18)
    for i in range (0,20):
        laberinto[i][ran] = '*'
    #Camino ultrahorizontal
    ran = random.randint(2,18)
    for i in range (0,20):
        laberinto[ran][i] = '*'

def imprimelab(laberinto):
    for i in range(0,20):
        print(laberinto[i])

def generlab(vertical,horizontal):
    laberinto = []
    for i in range (0,horizontal):
        laberinto.append(["_"]*vertical)
    return laberinto

resp = str #Variable respuesta

while resp != 'n' and resp != 'N':
    
    cont = 1 #Variable contador
    os.system ("clear")
    laberinto = generlab(20,20) #Funcion que genera laberinto
    genercamin(0,20)
    generconex(20,20) #Funcion que genera caminos
    print('\nTurno: 1')
    imprimelab(laberinto)
    
    while laberinto[0][0] != 'X':
        direc = str(input('¿En que dirección se movera?\n')) #pedir dirección

        if (direc == 'W' or direc == 'w' or direc == 'S' or 
            direc == 's' or direc == 'A' or direc == 'a' or 
            direc == 'D' or direc == 'd'):

            cont = cont + 1  #contador de turnos en ejecucion
            movdirec(direc)
            print('\nTurno: ',cont)
        else:
            os.system ("clear")
            print('El valor no es valido') #valor ingresado es invalido
            print('\nTurno: ',cont)

        imprimelab(laberinto)

        resp = 'e'
    while resp != 's' and resp != 'S' and resp != 'n' and resp != 'N':

        resp = str(input('¿Desea recorrer denuevo el laberinto? (s/n)\n'))
        
        if resp != 's' and resp != 'S' and resp != 'n' and resp != 'N':
            os.system ("clear")
            print('Caracter invalido, ingrese "s" o "n"...')