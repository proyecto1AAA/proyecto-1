resp = str
while resp != 'n' and resp != 'N':

    cont = 1 #turnos
    a = 1 #movimiento de cordenadas

    #Todos los corde (de cordenada) forman al laberinto

    corde0 = ['F','*','X','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde1 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde2 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde3 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde4 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde5 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde6 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde7 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde8 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde9 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde10 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde11 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde12 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde13 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde14 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde15 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde16 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde17 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde18 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]
    corde19 = ['*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*','*',]

    print('Turno: ',cont)
    print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
    #Imprime el laberinto

    def direccionalW(a):
        numW = int(0) #Busca a X en las cordenadas 0
        try:
            numW = corde0.index('X')
            print('\nTurno: ',cont)
            print('Ha chocado con una pared del laberinto\n\n')
        except:
            print('\nSe movio hacia arriba\n')
            if corde1.count('X') == 1:
                numW = corde1.index('X')
                if corde0[numW] == '*':
                    corde0[numW] = 'X'
                    corde1[numW] = '*'
                    print('Turno: ',cont)
                    print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
                else:
                    print('No hay camino disponible')
            if corde2.count('X') == 1:
                numW = corde2.index('X')
                corde1[numW] = 'X'
                corde2[numW] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde3.count('X') == 1:
                numW = corde3.index('X')
                corde2[numW] = 'X'
                corde3[numW] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde4.count('X') == 1: 
                numW = corde4.index('X')
                corde3[numW] = 'X'
                corde4[numW] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)      
            if corde5.count('X') == 1:
                numW = corde5.index('X')
                corde4[numW] = 'X'
                corde5[numW] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde6.count('X') == 1:
                numW = corde6.index('X')
                corde5[numW] = 'X'
                corde6[numW] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde7.count('X') == 1:
                numW = corde7.index('X')
                corde6[numW] = 'X'
                corde7[numW] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde8.count('X') == 1: 
                numW = corde8.index('X')
                corde7[numW] = 'X'
                corde8[numW] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde9.count('X') == 1:
                numW = corde9.index('X')
                corde8[numW] = 'X'
                corde9[numW] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde10.count('X') == 1:
                numW = corde10.index('X')
                corde9[numW] = 'X'
                corde10[numW] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde11.count('X') == 1:
                numW = corde11.index('X')
                corde10[numW] = 'X'
                corde11[numW] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde12.count('X') == 1:
                numW = corde12.index('X')
                corde11[numW] = 'X'
                corde12[numW] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde13.count('X') == 1:
                numW = corde13.index('X')
                corde12[numW] = 'X'
                corde13[numW] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde14.count('X') == 1:
                numW = corde14.index('X')
                corde13[numW] = 'X'
                corde14[numW] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde15.count('X') == 1:
                numW = corde15.index('X')
                corde14[numW] = 'X'
                corde15[numW] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde16.count('X') == 1:
                numW = corde16.index('X')
                corde15[numW] = 'X'
                corde16[numW] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde17.count('X') == 1:
                numW = corde17.index('X')
                corde16[numW] = 'X'
                corde17[numW] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde18.count('X') == 1:
                numW = corde18.index('X')
                corde17[numW] = 'X'
                corde18[numW] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde19.count('X') == 1:
                numW = corde19.index('X')
                corde18[numW] = 'X'
                corde19[numW] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)

    def direccionalS(a):
        numS = int(0) #Busca a X en las cordenadas 19
        try:
            numS = corde19.index('X')
            print('\nTurno: ',cont)
            print('Ha chocado con una pared del laberinto\n\n')
        except:
            print('\nSe movio hacia abajo\n')
            if corde18.count('X') == 1:
                numS = corde18.index('X')
                if corde19[numS] == '*':
                    corde19[numS] = 'X'
                    corde18[numS] = '*'
                    print('Turno: ',cont)
                    print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
                else:
                    print('No hay camino disponible')
            if corde17.count('X') == 1:
                numS = corde17.index('X')
                if corde18[numS] == '*':
                    corde18[numS] = 'X'
                    corde17[numS] = '*'
                    print('Turno: ',cont)
                    print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
                else:
                    print('No hay camino disponible')
            if corde16.count('X') == 1:
                numS = corde16.index('X')
                corde17[numS] = 'X'
                corde16[numS] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde15.count('X') == 1:
                numS = corde15.index('X')
                corde16[numS] = 'X'
                corde15[numS] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde14.count('X') == 1:
                numS = corde14.index('X')
                corde15[numS] = 'X'
                corde14[numS] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde13.count('X') == 1:
                numS = corde13.index('X')
                corde14[numS] = 'X'
                corde13[numS] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)            
            if corde12.count('X') == 1:
                numS = corde12.index('X')
                corde13[numS] = 'X'
                corde12[numS] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)            
            if corde11.count('X') == 1:
                numS = corde11.index('X')
                corde12[numS] = 'X'
                corde11[numS] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)            
            if corde10.count('X') == 1:
                numS = corde10.index('X')
                corde11[numS] = 'X'
                corde10[numS] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)            
            if corde9.count('X') == 1:
                numS = corde9.index('X')
                corde10[numS] = 'X'
                corde9[numS] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)            
            if corde8.count('X') == 1: 
                numS = corde8.index('X')
                corde9[numS] = 'X'
                corde8[numS] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)            
            if corde7.count('X') == 1:
                numS = corde7.index('X')
                corde8[numS] = 'X'
                corde7[numS] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)            
            if corde6.count('X') == 1:
                numS = corde6.index('X')
                corde7[numS] = 'X'
                corde6[numS] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)            
            if corde5.count('X') == 1:
                numS = corde5.index('X')
                corde6[numS] = 'X'
                corde5[numS] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)            
            if corde4.count('X') == 1: 
                numS = corde4.index('X')
                corde5[numS] = 'X'
                corde4[numS] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)            
            if corde3.count('X') == 1:
                numS = corde3.index('X')
                corde4[numS] = 'X'
                corde3[numS] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)            
            if corde2.count('X') == 1:
                numS = corde2.index('X')
                corde3[numS] = 'X'
                corde2[numS] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)            
            if corde1.count('X') == 1:
                numS = corde1.index('X')
                corde2[numS] = 'X'
                corde1[numS] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)            
            if corde0.count('X') == 1:
                numS = corde0.index('X')
                corde1[numS] = 'X'
                corde0[numS] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)

    def direccionalA(a):
        numA = int(0) #Busca a X en las cordenadas 0
        if corde0[00] == 'X' or corde1[00] == 'X' or corde2[00] == 'X' or corde3[00] == 'X' or corde4[00] == 'X' or corde5[00] == 'X' or corde6[00] == 'X' or corde7[00] == 'X' or corde8[00] == 'X' or corde9[00] == 'X' or corde10[00] == 'X' or corde11[00] == 'X' or corde12[00] == 'X' or corde13[00] == 'X' or corde14[00] == 'X' or corde15[00] == 'X' or corde16[00] == 'X' or corde17[00] == 'X' or corde18[00] == 'X' or corde19[00] == 'X':
            print('\nTurno: ',cont)
            print('Ha chocado con una pared del laberinto\n\n')
        else:
            print('\nSe movio hacia la izquierda\n')
            if corde0.count('X') == 1:
                numA = corde0.index('X')
                if corde0[numA-a] == '*':
                    corde0[numA-a] = 'X'
                    corde0[numA] = '*'
                    print('Turno: ',cont)
                    print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
                else:
                    print('No hay camino disponible')
            if corde1.count('X') == 1:
                numA = corde1.index('X')
                corde1[numA-a] = 'X'
                corde1[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde2.count('X') == 1:
                numA = corde2.index('X')
                corde2[numA-a] = 'X'
                corde2[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde3.count('X') == 1:
                numA = corde3.index('X')
                corde3[numA-a] = 'X'
                corde3[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde4.count('X') == 1: 
                numA = corde4.index('X')
                corde4[numA-a] = 'X'
                corde4[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)    
            if corde5.count('X') == 1:
                numA = corde5.index('X')
                corde5[numA-a] = 'X'
                corde5[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde6.count('X') == 1:
                numA = corde6.index('X')
                corde6[numA-a] = 'X'
                corde6[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde7.count('X') == 1:
                numA = corde7.index('X')
                corde7[numA-a] = 'X'
                corde7[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde8.count('X') == 1: 
                numA = corde8.index('X')
                corde8[numA-a] = 'X'
                corde8[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde9.count('X') == 1:
                numA = corde9.index('X')
                corde9[numA-a] = 'X'
                corde9[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde10.count('X') == 1:
                numA = corde10.index('X')
                corde10[numA-a] = 'X'
                corde10[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde11.count('X') == 1:
                numA = corde11.index('X')
                corde11[numA-a] = 'X'
                corde11[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde12.count('X') == 1:
                numA = corde12.index('X')
                corde12[numA-a] = 'X'
                corde12[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde13.count('X') == 1:
                numA = corde13.index('X')
                corde13[numA-a] = 'X'
                corde13[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde14.count('X') == 1:
                numA = corde14.index('X')
                corde14[numA-a] = 'X'
                corde14[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde15.count('X') == 1:
                numA = corde15.index('X')
                corde15[numA-a] = 'X'
                corde15[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde16.count('X') == 1:
                numA = corde16.index('X')
                corde16[numA-a] = 'X'
                corde16[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde17.count('X') == 1:
                numA = corde17.index('X')
                corde17[numA-a] = 'X'
                corde17[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde18.count('X') == 1:
                numA = corde18.index('X')
                corde18[numA-a] = 'X'
                corde18[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde19.count('X') == 1:
                numA = corde19.index('X')
                corde19[numA-a] = 'X'
                corde19[numA] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)

    def direccionalD(a):
        numD = int(0) #Busca a X en las cordenadas 0
        if corde0[19] == 'X' or corde1[19] == 'X' or corde2[19] == 'X' or corde3[19] == 'X' or corde4[19] == 'X' or corde5[19] == 'X' or corde6[19] == 'X' or corde7[19] == 'X' or corde8[19] == 'X' or corde9[19] == 'X' or corde10[19] == 'X' or corde11[19] == 'X' or corde12[19] == 'X' or corde13[19] == 'X' or corde14[19] == 'X' or corde15[19] == 'X' or corde16[19] == 'X' or corde17[19] == 'X' or corde18[19] == 'X' or corde19[19] == 'X':
            print('\nTurno: ',cont)
            print('Ha chocado con una pared del laberinto\n\n')
        else:
            print('\nSe movio hacia la derecha\n')
            if corde0.count('X') == 1:
                numD = corde0.index('X')
                corde0[numD+a] = 'X'
                corde0[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde1.count('X') == 1:
                numD = corde1.index('X')
                corde1[numD+a] = 'X'
                corde1[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde2.count('X') == 1:
                numD = corde2.index('X')
                corde2[numD+a] = 'X'
                corde2[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde3.count('X') == 1:
                numD = corde3.index('X')
                corde3[numD+a] = 'X'
                corde3[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde4.count('X') == 1: 
                numD = corde4.index('X')
                corde4[numD+a] = 'X'
                corde4[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)    
            if corde5.count('X') == 1:
                numD = corde5.index('X')
                corde5[numD+a] = 'X'
                corde5[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde6.count('X') == 1:
                numD = corde6.index('X')
                corde6[numD+a] = 'X'
                corde6[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde7.count('X') == 1:
                numD = corde7.index('X')
                corde7[numD+a] = 'X'
                corde7[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde8.count('X') == 1: 
                numD = corde8.index('X')
                corde8[numD+a] = 'X'
                corde8[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde9.count('X') == 1:
                numD = corde9.index('X')
                corde9[numD+a] = 'X'
                corde9[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde10.count('X') == 1:
                numD = corde10.index('X')
                corde10[numD+a] = 'X'
                corde10[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde11.count('X') == 1:
                numD = corde11.index('X')
                corde11[numD+a] = 'X'
                corde11[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde12.count('X') == 1:
                numD = corde12.index('X')
                corde12[numD+a] = 'X'
                corde12[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde13.count('X') == 1:
                numD = corde13.index('X')
                corde13[numD+a] = 'X'
                corde13[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde14.count('X') == 1:
                numD = corde14.index('X')
                corde14[numD+a] = 'X'
                corde14[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde15.count('X') == 1:
                numD = corde15.index('X')
                corde15[numD+a] = 'X'
                corde15[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde16.count('X') == 1:
                numD = corde16.index('X')
                corde16[numD+a] = 'X'
                corde16[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde17.count('X') == 1:
                numD = corde17.index('X')
                corde17[numD+a] = 'X'
                corde17[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde18.count('X') == 1:
                numD = corde18.index('X')
                corde18[numD+a] = 'X'
                corde18[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
            if corde19.count('X') == 1:
                numD = corde19.index('X')
                corde19[numD+a] = 'X'
                corde19[numD] = '*'
                print('Turno: ',cont)
                print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)
    
    while corde0[00] != 'X':
        direc = str(input('¿En que dirección se movera?\n')) #pedir dirección

        if direc == 'W' or direc == 'w' or direc == 'S' or direc == 's' or direc == 'A' or direc == 'a' or direc == 'D' or direc == 'd':
            cont = cont + 1  #contador de turnos

            if direc == 'W' or direc == 'w': #direccional arriba
                direccionalW(a)
            if direc == 'S' or direc == 's': #direccional abajo
                direccionalS(a)
            if direc == 'A' or direc == 'a': #direccional izquierda
                direccionalA(a)
            if direc == 'D' or direc == 'd': #direcional derecha
                direccionalD(a)

        else:
            print('El valor no es valido\n') #valor ingresado es invalido
            cont = cont  #contador de turnos

    resp = str(input('¿Desea recorrer denuevo el laberinto? (s/n)\n'))