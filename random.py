import random

corde0 = ['F','*','*','*','*','*','*','*','*','*','_','_','_','_','_','_','_','_','_','_']
corde1 = ['_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_']
corde2 = ['_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_']
corde3 = ['_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_']
corde4 = ['_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_']
corde5 = ['_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_']
corde6 = ['_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_']
corde7 = ['_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_']
corde8 = ['_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_']
corde9 = ['_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_']
corde10 = ['_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_']
corde11 = ['_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_']
corde12 = ['_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_']
corde13 = ['_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_']
corde14 = ['_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_']
corde15 = ['_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_']
corde16 = ['_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_']
corde17 = ['_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_']
corde18 = ['_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_']
corde19 = ['_','_','_','_','_','_','_','_','_','*','*','*','*','*','*','*','*','*','*','X']

#Horizontal
for e in range(0,2):

    ran = random.randint(2,17)

    if ran == 2:
        if corde1.count('*') != 19 and corde3.count('*') != 19:
            ran = random.randint(1,2)
            if ran == 1:
                for i in range(0,9):
                    corde2[i] = '*'
            else:
                for i in range(10,19):
                    corde2[i] = '*'
        else:
            e = e-1

    if ran == 3:
        if corde2.count('*') != 19 and corde4.count('*') != 19:
            ran = random.randint(1,2)
            if ran == 1:
                for i in range(0,9):
                    corde3[i] = '*'
            else:
                for i in range(10,19):
                    corde3[i] = '*'
        else:
            e = e-1

    if ran == 4:
        if corde3.count('*') != 19 and corde5.count('*') != 19:
            ran = random.randint(1,2)
            if ran == 1:
                for i in range(0,9):
                    corde4[i] = '*'
            else:
                for i in range(10,19):
                    corde4[i] = '*'
        else:
            e = e-1

    if ran == 5:
        if corde4.count('*') != 19 and corde6.count('*') != 19:
            ran = random.randint(1,2)
            if ran == 1:
                for i in range(0,9):
                    corde5[i] = '*'
            else:
                for i in range(10,19):
                    corde5[i] = '*'
        else:
            e = e-1

    if ran == 6:
        if corde5.count('*') != 19 and corde7.count('*') != 19:
            ran = random.randint(1,2)
            if ran == 1:
                for i in range(0,9):
                    corde6[i] = '*'
            else:
                for i in range(10,19):
                    corde6[i] = '*'
        else:
            e = e-1
            
    if ran == 7:
        if corde6.count('*') != 19 and corde8.count('*') != 19:
            ran = random.randint(1,2)
            if ran == 1:
                for i in range(0,9):
                    corde7[i] = '*'
            else:
                for i in range(10,19):
                    corde7[i] = '*'
        else:
            e = e-1

    if ran == 8:
        if corde7.count('*') != 19 and corde9.count('*') != 19:
            ran = random.randint(1,2)
            if ran == 1:
                for i in range(0,9):
                    corde8[i] = '*'
            else:
                for i in range(10,19):
                    corde8[i] = '*'
        else:
            e = e-1

    if ran == 9:
        if corde8.count('*') != 19 and corde10.count('*') != 19:
            ran = random.randint(1,2)
            if ran == 1:
                for i in range(0,9):
                    corde9[i] = '*'
            else:
                for i in range(10,19):
                    corde9[i] = '*'
        else:
            e = e-1

    if ran == 10:
        if corde9.count('*') != 19 and corde11.count('*') != 19:
            ran = random.randint(1,2)
            if ran == 1:
                for i in range(0,9):
                    corde10[i] = '*'
            else:
                for i in range(10,19):
                    corde10[i] = '*'
        else:
            e = e-1

    if ran == 11:
        if corde10.count('*') != 19 and corde12.count('*') != 19:
            ran = random.randint(1,2)
            if ran == 1:
                for i in range(0,9):
                    corde11[i] = '*'
            else:
                for i in range(10,19):
                    corde11[i] = '*'
        else:
            e = e-1

    if ran == 12:
        if corde11count('*') != 19 and corde13.count('*') != 19:
            ran = random.randint(1,2)
            if ran == 1:
                for i in range(0,9):
                    corde12[i] = '*'
            else:
                for i in range(10,19):
                    corde12[i] = '*'
        else:
            e = e-1

    if ran == 13:
        if corde12.count('*') != 19 and corde14.count('*') != 19:
            ran = random.randint(1,2)
            if ran == 1:
                for i in range(0,9):
                    corde13[i] = '*'
            else:
                for i in range(10,19):
                    corde13[i] = '*'
        else:
            e = e-1

    if ran == 14:
        if corde13.count('*') != 19 and corde15.count('*') != 19:
            ran = random.randint(1,2)
            if ran == 1:
                for i in range(0,9):
                    corde14[i] = '*'
            else:
                for i in range(10,19):
                    corde14[i] = '*'
        else:
            e = e-1

    if ran == 15:
        if corde14.count('*') != 19 and corde16.count('*') != 19:
            ran = random.randint(1,2)
            if ran == 1:
                for i in range(0,9):
                    corde15[i] = '*'
            else:
                for i in range(10,19):
                    corde15[i] = '*'
        else:
            e = e-1

    if ran == 16:
        if corde15.count('*') != 19 and corde17.count('*') != 19:
            ran = random.randint(1,2)
            if ran == 1:
                for i in range(0,9):
                    corde16[i] = '*'
            else:
                for i in range(10,19):
                    corde16[i] = '*'
        else:
            e = e-1
            
    if ran == 17:
        if corde16.count('*') != 19 and corde18.count('*') != 19:
            ran = random.randint(1,2)
            if ran == 1:
                for i in range(0,9):
                    corde17[i] = '*'
            else:
                for i in range(10,19):
                    corde17[i] = '*'
        else:
            e = e-1

#Vertical
for i in range(0,1):
    ran = random.randint(1,18)
    
    corde0[ran] = '*'
    corde1[ran] = '*'
    corde2[ran] = '*'
    corde3[ran] = '*'
    corde4[ran] = '*'
    corde5[ran] = '*'
    corde6[ran] = '*'
    corde7[ran] = '*'
    corde8[ran] = '*'
    corde9[ran] = '*'          
    corde10[ran] = '*'
    corde11[ran] = '*'
    corde11[ran] = '*'
    corde12[ran] = '*'
    corde13[ran] = '*'
    corde14[ran] = '*'
    corde15[ran] = '*'
    corde16[ran] = '*'
    corde17[ran] = '*'
    corde18[ran] = '*'
    corde19[ran] = '*'

for i in range(0,2):

    ran = random.randint(1,9)
    if corde4[ran-1] != '*' and corde4[ran+1] != '*':

        corde0[ran] = '*'
        corde1[ran] = '*'
        corde2[ran] = '*'
        corde3[ran] = '*'
        corde4[ran] = '*'
        corde5[ran] = '*'
        corde6[ran] = '*'
        corde7[ran] = '*'
        corde8[ran] = '*'
        corde9[ran] = '*'
    else:
        i = i-1

    ran = random.randint(1,9)
    if corde14[ran-1] != '*' and corde14[ran+1] != '*':

        corde10[ran] = '*'
        corde11[ran] = '*'
        corde11[ran] = '*'
        corde12[ran] = '*'
        corde13[ran] = '*'
        corde14[ran] = '*'
        corde15[ran] = '*'
        corde16[ran] = '*'
        corde17[ran] = '*'
        corde18[ran] = '*'
        corde19[ran] = '*'
    else:
        i = i-1

    ran = random.randint(10,18)
    if corde4[ran-1] != '*' and corde4[ran+1] != '*':

        corde0[ran] = '*'
        corde1[ran] = '*'
        corde2[ran] = '*'
        corde3[ran] = '*'
        corde4[ran] = '*'
        corde5[ran] = '*'
        corde6[ran] = '*'
        corde7[ran] = '*'
        corde8[ran] = '*'
        corde9[ran] = '*'
    else:
        i = i-1

    ran = random.randint(10,18)
    if corde14[ran-1] != '*' and corde14[ran+1] != '*':

        corde10[ran] = '*'
        corde11[ran] = '*'
        corde11[ran] = '*'
        corde12[ran] = '*'
        corde13[ran] = '*'
        corde14[ran] = '*'
        corde15[ran] = '*'
        corde16[ran] = '*'
        corde17[ran] = '*'
        corde18[ran] = '*'
        corde19[ran] = '*'
    else:
        i = i-1


print('\n',corde0,'\n',corde1,'\n',corde2,'\n',corde3,'\n',corde4,'\n',corde5,'\n',corde6,'\n',corde7,'\n',corde8,'\n',corde9,'\n',corde10,'\n',corde11,'\n',corde12,'\n',corde13,'\n',corde14,'\n',corde15,'\n',corde16,'\n',corde17,'\n',corde18,'\n',corde19)